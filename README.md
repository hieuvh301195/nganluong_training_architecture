# NGANLUONG WALLET APP
---
INTRODUCTION
-------

**Nganluong.vn is the pioneer and leading online payment gateway** in Vietnam, in terms of products and services, market share coverage and payment volume. Developed by Nexttech Group (formerly PeaceSoft Group) since 2009, Ngan Luong allows individuals and businesses to send and receive money on the Internet quickly, safely and conveniently.

NOTE
-------

This is a ***demo project*** about **architecture** of Ngan Luong Wallet application with Flutter version. 

FEATURES
-------

We are focus on user experiences in this app. It has to be easy to use, even for those ones who never use any other wallet before. Therefore, we are providing three primary functions: 
- Deposit
- Transfer
- Widthraw

All transactions are made online via ATM, Visa, Master, JCB, Amex, Internet Banking, QR-Pay ... and other convenient forms.

FOR DEVELOPERS
-------

**Clone the project**

- Clone with HTTP: 
```bash
	$ git clone http://gitlab.saobang.vn/nganluong/app-nganluong-v2-flutter.git
```

- Clone with SSH: 
```bash
	$ git clone git@gitlab.saobang.vn:nganluong/app-nganluong-v2-flutter.git
```

**Install all dependencies**

First, you need to make sure that you already have Flutter installed. Otherwise, please install it follow this [document](https://flutter.dev/docs/get-started/install).
If you already have Flutter, then you can run this command after clone project:
```bash
    $ flutter pub get
```
    
**Running**

You can run it by virtual machine or by your real devices. Just turn on your virtual machine or plug your real device and then run: 
```bash
    $ flutter run 
```

That command will build a debug version and install it into your device. You can find debug version at [YOUR_PROJECT_NAME]/build/app/outputs/apk/debug/app-debug.apk

**Build a release version**
1. Build Android
+ Build APK (to install on Android devices)
```bash
    $ flutter build apk
```

+ Build AAB (Android App Bundle - for deloy in CH Play)
```bash
    $ flutter build appbundle
```

2. Build iOS (MacOS required)
+ Build IPA (to install on iOS devices)
```bash
    $ flutter build ios
```

For more information about building syntax, run:
```bash
    $ flutter build --help
```

LICENSE
-------
(updating....)

