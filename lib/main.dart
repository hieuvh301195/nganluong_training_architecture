import 'package:app_ngan_luong/router/route.dart';
import 'package:app_ngan_luong/ui/screens/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:app_ngan_luong/utils/custom_colors.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:app_ngan_luong/ui/screens/sign_up_screen.dart';
import 'package:app_ngan_luong/ui/screens/forgot_password_screen.dart';
import 'package:app_ngan_luong/ui/screens/login_screen.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:app_ngan_luong/store/language/languages.dart';

final languagetrans = LanguageTrans();

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    FlutterStatusbarcolor();

    return (new Material(
        child: MaterialApp(
          title: 'Flutter Demo',
            routes: NganLuongRoute.routes
        ),
      )
    );
    
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: ListView(
      children: <Widget>[
        LoginScreen()
      ],
    ));
  }
}
