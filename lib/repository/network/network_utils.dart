import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class NetworkUtils{

  static String deviceToken;
  String os = Platform.operatingSystem;
  Dio dio = Dio();
  static NetworkUtils _instance;

  static NetworkUtils getInstance(){
    if (_instance==null){
      _instance = NetworkUtils();
    }
    return _instance;
  }

  _getDeviceId() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (os == "android") {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      deviceToken = androidDeviceInfo.androidId; // unique ID on Android
    } else {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      deviceToken = iosDeviceInfo.identifierForVendor; // unique ID on iOS
    }
    deviceToken += os;
  }

  _getHeaders() {
    _getDeviceId();
    dio.options.headers = {
      "Accept": "application/json",
      "device-token": deviceToken,
    };
  }

  Future<dynamic> postAPI (String url, {Map header, body}) async {
    Map data = {
      header: header,
      body:body
    };
    _getHeaders();
    var response = await dio.post(url, data: json.encode(data));

    if (response.statusCode<200 || response.statusCode>400){
      print(response);
      throw "Error when call POST method";
    }else{
      print(response);
    }
  }

  Future<dynamic> getAPI (String url, {Map header, body}) async {
    Map data = {
      header: header,
      body:body
    };
    _getHeaders();
    var response = await dio.get(url);

    if (response.statusCode<200 || response.statusCode>400){
      print(response);
      throw "Error when call POST method";
    }else{
      return response.data.toString();
    }
  }
}