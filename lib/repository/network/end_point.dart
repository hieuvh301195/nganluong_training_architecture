class EndPoint{
  EndPoint._();

  static const String BASE_URL = "https://devapi-app.nganluong.vn/v1/";
  static const String LOGIN_URL = BASE_URL + "users/login";
  static const String CREATE_ACCOUNT_URL = BASE_URL + "users/register";

}