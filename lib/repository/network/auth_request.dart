import 'dart:io' show Platform;
import 'dart:async';
import 'package:app_ngan_luong/router/route.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';

import 'network_utils.dart';
import 'end_point.dart';

class AuthRequest {
  NetworkUtils _networkUtils = NetworkUtils.getInstance();

  static final apiUrl = "https://devapi-app.nganluong.vn/v1/";
  String code;
  String message;
  String data;
  String successfulCode = "0";

  static AuthRequest _instance;

  static AuthRequest getInstance() {
    if (_instance == null) {
      _instance = AuthRequest();
    }
    return _instance;
  }



  void login(String username, String password, BuildContext context) async {
    String url = EndPoint.LOGIN_URL;
    Map<String, dynamic> body = {"email": username, "password": password};
    var response = await _networkUtils.postAPI(url, body: body);
    code = response['code'].toString();
    message = response['message'].toString();
    data = response['data'].toString();
    print(response);
    if (code == successfulCode) {
      NganLuongRoute.setHomeScreenAsRoot(context);
    }
  }
}
