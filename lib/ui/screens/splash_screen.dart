import 'dart:async';

import 'package:app_ngan_luong/ui/screens/login_screen.dart';
import 'package:flutter/material.dart';



class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final String urlBackground = "lib/res/images/splash/splash_background.png";
  final String urlLogo = "lib/res/images/splash/logo.png";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _startTime();
   }

  _startTime() async {
    var _duration = new Duration(seconds: 1);
    return new Timer(_duration, _navigationPage);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(urlBackground),
          fit: BoxFit.cover,
        ),
      ),
      child: Center(
        child: Image(
          image: AssetImage(urlLogo),
          width: MediaQuery.of(context).size.width*0.85,
        ),
      ),
    );
  }


  void _navigationPage() {
    Navigator.of(context).pushReplacementNamed('/login');
  }
}

