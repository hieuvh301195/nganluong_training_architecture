import 'package:app_ngan_luong/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:touchable_opacity/touchable_opacity.dart';

import 'package:app_ngan_luong/ui/common_widgets/choose_language.dart';
import 'package:app_ngan_luong/utils/custom_colors.dart';
import 'package:app_ngan_luong/utils/common_styles.dart';
import 'package:app_ngan_luong/validate/validator.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    final _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: _screenSize.height * 0.5,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5.0),
                          child: TouchableOpacity(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: IconButton(
                              icon: Icon(
                                Icons.arrow_back,
                                color: Colors.black,
                                size: 26.0,
                              ),
                              onPressed: null,
                            ),
                          ),
                        ),
                        Observer(
                          builder: (_) => Text(
                            languagetrans.strings.getString("FORGOT_PASSWORD"),
                            style: CommonStyles.title(),
                            textAlign: TextAlign.center,
                            strutStyle: StrutStyle(height: 4),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                          child: Observer(
                            builder: (_) => Text(
                              languagetrans.strings
                                  .getString("FORGOT_PASSWORD_CHOOSE_METHOD"),
                              style: CommonStyles.textNormal(),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 10),
                          // color: Colors.white,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.1),
                                blurRadius:
                                    50.0, // has the effect of softening the shadow
                                spreadRadius:
                                    0.0, // has the effect of extending the shadow
                                offset: Offset(
                                  0.0, // horizontal, move right 10
                                  10.0, // vertical, move down 10
                                ),
                              )
                            ],
                            borderRadius:
                                new BorderRadius.all(Radius.circular(10)),
                          ),
                          child: Container(
                            child: Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(10, 20, 20, 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: ImageIcon(
                                          AssetImage(
                                              "lib/res/images/image_icons/enve.png"),
                                          color: Colors.black,
                                          size: 50.0,
                                        ),
                                      ),
                                      Observer(
                                        builder: (_) => Container(
                                          width: _screenSize.width * 0.52,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.max,
                                            children: [
                                              Text(
                                                languagetrans.strings.getString(
                                                    'FORGOT_PASSWORD_NOTI'),
                                                style:
                                                    CommonStyles.textNormal(),
                                                textAlign: TextAlign.left,
                                              ),
                                              Form(
                                                key: _formKey,
                                                child: Container(
                                                  child: Stack(
                                                    children: <Widget>[
                                                      TextFormField(
                                                        decoration:
                                                            InputDecoration(
                                                          hintText: languagetrans
                                                              .strings
                                                              .getString(
                                                                  'TYPE_EMAIL'),
                                                          hintStyle: CommonStyles
                                                              .placeholderNormal(),
                                                          contentPadding:
                                                              const EdgeInsetsDirectional
                                                                      .fromSTEB(
                                                                  0,
                                                                  0,
                                                                  20,
                                                                  -10),
                                                        ),
                                                        validator: (value) {
                                                          if (value.isEmpty) {
                                                            return languagetrans
                                                                .strings
                                                                .getString(
                                                                    "EMAIL_EMPTY");
                                                          } else if (!Validator
                                                              .isEmail(value)) {
                                                            return languagetrans
                                                                .strings
                                                                .getString(
                                                                    "EMAIL_INVALID");
                                                          }
                                                          return null;
                                                        },
                                                      ),
                                                      Positioned(
                                                        top: 5,
                                                        right: -15,
                                                        child: TouchableOpacity(
                                                          onTap: () {
                                                            if (_formKey
                                                                .currentState
                                                                .validate()) {}
                                                          },
                                                          child: IconButton(
                                                            icon: Icon(
                                                              Icons
                                                                  .arrow_forward,
                                                              color: const Color(
                                                                  0xfffb8732),
                                                              size: 25.0,
                                                            ),
                                                            onPressed: null,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: _screenSize.height * 0.45,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: ChooseLanguage(),
                        ),
                        Observer(
                          builder: (_) => Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                                child: Text(
                                  languagetrans.strings
                                      .getString("CONTACT_HELP")
                                      .toUpperCase(),
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'NunitoSans',
                                    color: CustomColor.fromHex('#0f2539'),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
