import 'package:app_ngan_luong/main.dart';
import 'package:app_ngan_luong/ui/common_widgets/button_orange.dart';
import 'package:app_ngan_luong/ui/common_widgets/choose_language.dart';
import 'package:app_ngan_luong/ui/common_widgets/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:touchable_opacity/touchable_opacity.dart';
import 'package:app_ngan_luong/utils/strings.dart';
import 'package:app_ngan_luong/utils/common_styles.dart';
import 'package:app_ngan_luong/utils/custom_colors.dart';
import 'package:app_ngan_luong/ui/common_widgets/custom_checkbox.dart';
import 'package:app_ngan_luong/validate/validator.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  bool usePhoneToLogin;
  bool agreeToTerms;
  bool isReadyToLogin;
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneNumberController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmController = TextEditingController();

  _signUp() {
    print('execution');
    print('name: ' + nameController.text);
    print('email: ' + emailController.text);
    print('password: ' + passwordController.text);
    print('confirm: ' + confirmController.text);
    print('phone: ' + phoneNumberController.text);
    print('use this phone to login: ' + usePhoneToLogin.toString());
    print('agree to terms: ' + agreeToTerms.toString());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    usePhoneToLogin = true;
    agreeToTerms = false;
    isReadyToLogin = false;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double statusBarHeight = MediaQuery.of(context).padding.top;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
          width: width,
          height: height,
          padding: EdgeInsets.only(top: statusBarHeight * 2, bottom: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _renderGoBackIcon(context),
              _renderText(context),
              _renderTextInputs(context),
              ChooseLanguage(),
              SizedBox(
                height: 10,
              ),
              _renderButton(context),
            ],
          )),
    );
  }

  Widget _renderGoBackIcon(BuildContext context) {
    double size = 22.0;
    return TouchableOpacity(
      onTap: () {
        Navigator.pop(context);
      },
      child: Image(
        image: AssetImage("lib/res/images/login/beck.png"),
        width: size,
        height: size,
      ),
    );
  }

  Widget _renderText(BuildContext context) {
    return Observer(
      builder: (_) => Container(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 25.0,
            ),
            Text(
              languagetrans.strings.getString("CREATE_ACCOUNT"),
              style: CommonStyles.title(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.0),
              child: Text(
                languagetrans.strings.getString('CREATE_ACCOUNT_DESCRIPTION'),
                style: CommonStyles.textNormal(),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _renderTextInputs(BuildContext context) {
    return Expanded(
      key: Key("listSignUp"),
      child: Observer(
        builder: (_) => Container(
          margin: EdgeInsets.symmetric(horizontal: 28.0),
          child: ListView(
            children: <Widget>[
              TextInput(
                label: "YOUR_NAME",
                placeholder: "TYPE_YOUR_NAME",
                controller: nameController,
              ),
              TextInput(
                label: "EMAIL",
                placeholder: "TYPE_EMAIL",
                type: TextInputType.emailAddress,
                controller: emailController,
              ),
              TextInput(
                label: "PHONE_NUMBER",
                placeholder: "TYPE_PHONE_NUMBER",
                type: TextInputType.number,
                controller: phoneNumberController,
              ),
              CustomCheckBox(
                  text: "USE_PHONE_NUMBER_TO_LOGIN",
                  isChecked: usePhoneToLogin),
              TextInput(
                label: "PASSWORD",
                placeholder: "TYPE_PASSWORD",
                obscureText: true,
                controller: passwordController,
              ),
              TextInput(
                label: "CONFIRM_PASSWORD",
                placeholder: "CONFIRM_PASSWORD",
                obscureText: true,
                controller: confirmController,
              ),
              CustomCheckBox(
                text: 'AGREE_TO_TERM_AGREEMENT',
                isChecked: agreeToTerms,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _renderButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 28.0),
      child: ButtonOrange(
        text: "SIGNUP",
        onPress: () {
          if (!Validator.isEmail(emailController.text)) {
            print("this is not email");
          }
        },
        isReady: true,
      ),
    );
  }
}

class TextInput extends StatefulWidget {
  String label;
  String placeholder;
  TextInputType type;
  bool obscureText;
  TextEditingController controller;

  TextInput(
      {@required this.label,
      @required this.placeholder,
      @required this.controller,
      this.type,
      this.obscureText});

  _TextInput createState() => _TextInput(
      label: this.label,
      placeholder: this.placeholder,
      type: this.type,
      controller: this.controller,
      obscureText: this.obscureText);
}

class _TextInput extends State<TextInput> {
  String label;
  String placeholder;
  TextInputType type;
  bool obscureText;
  TextEditingController controller;

  _TextInput(
      {@required this.label,
      @required this.placeholder,
      @required this.controller,
      this.type,
      this.obscureText});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.8;
    double height = MediaQuery.of(context).size.height / 15;
    return Observer(
      builder: (_) => Container(
        width: width,
        height: height,
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        margin: EdgeInsets.only(bottom: 5.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Colors.white,
            boxShadow: <BoxShadow>[CommonStyles.shadowNormal()]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              languagetrans.strings.getString(label),
              style: CommonStyles.textNormal(),
            ),
            Container(
              width: width / 2,
              child: TextFormField(
                controller: controller,
                style: CommonStyles.textNormal(),
                decoration: InputDecoration(
                  hintText: languagetrans.strings.getString(placeholder),
                  hintStyle: CommonStyles.placeholderNormal(),
                  border: InputBorder.none,
                ),
                textAlign: TextAlign.right,
                textCapitalization: (type == null || type == TextInputType.text)
                    ? TextCapitalization.words
                    : TextCapitalization.none,
                keyboardType: (type == null || type == TextInputType.text)
                    ? TextInputType.text
                    : type,
                obscureText: (obscureText == null || obscureText == false)
                    ? false
                    : true,
                validator: (value) {
                  if (value == null || value == "") {
                    return "You can't leave that field empty";
                  } else if (type == TextInputType.emailAddress &&
                      !Validator.isEmail(controller.text)) {
                    return "This is not an email";
                  }
                  return null;
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
