
import 'package:app_ngan_luong/main.dart';
import 'package:app_ngan_luong/repository/network/auth_request.dart';
import 'package:app_ngan_luong/ui/common_widgets/choose_language.dart';
import 'package:app_ngan_luong/ui/common_widgets/button_orange.dart';
import 'package:app_ngan_luong/ui/common_widgets/loading_dialog.dart';
import 'package:app_ngan_luong/ui/screens/forgot_password_screen.dart';
import 'package:app_ngan_luong/ui/screens/home_screen.dart';
import 'package:app_ngan_luong/repository/network/end_point.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:touchable_opacity/touchable_opacity.dart';
import 'package:app_ngan_luong/utils/strings.dart';
import 'package:app_ngan_luong/utils/common_styles.dart';
import 'package:app_ngan_luong/utils/custom_colors.dart';
import 'package:app_ngan_luong/ui/screens/sign_up_screen.dart';
import 'package:app_ngan_luong/ui/common_widgets/custom_checkbox.dart';
import 'package:app_ngan_luong/validate/validator.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatelessWidget {
  bool isRemember;
  bool isReadyToLogin;
  final emailOrPhoneController = TextEditingController();
  final passwordController = TextEditingController();
  bool isEmailOrPhoneValidated = true;
  bool isPasswordValidated = true;

  @override
  void initState() {
    // TODO: implement initState
    // super.initState();
    isRemember = true;
    isReadyToLogin = false;
    isEmailOrPhoneValidated = true;
    isEmailOrPhoneValidated = true;
  }

//  successful code, but this code break architecture because it's not separate UI and logic

//  void _login(context) async {
//    String username = emailOrPhoneController.text;
//    String password = passwordController.text;
//    if (Validator.isEmail(username)){
//      Dio dio = Dio();
//      dio.options.headers = {
//        "Content-Type": "application/json",
//        "device-token": "unkown",
//      };
//      Map data = {
//        "email":username,
//        "password":password
//      };
//      Response response = await dio.post(EndPoint.LOGIN_URL, data: data);
//      print(response);
//      if (response.data['code']==0){
//        _navigateToHomeScreen(context);
//      }
//    }
//  }


  // this code follow architecture but it's not work yet
  void _login(context) async {
    String username = emailOrPhoneController.text;
    String password = passwordController.text;
    if (Validator.isEmail(username)){
      AuthRequest authRequest = AuthRequest.getInstance();
      authRequest.login(username, password, context);
    }
  }

  _navigateToHomeScreen(context) {
    Navigator.of(context).pushReplacementNamed('/home');
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double statusBarHeight = MediaQuery.of(context).padding.top;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: width,
        height: height,
        padding: EdgeInsets.only(top: statusBarHeight * 2, bottom: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _renderTitle(context),
            SizedBox(
              height: 30.0,
            ),
            _renderTextInputs(context),
            SizedBox(
              height: 16.0,
            ),
            _renderLoginButton(context),
            _renderOptions(context),
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  ChooseLanguage(),
                  SizedBox(
                    height: 20.0,
                  ),
                  _renderSignUpButton(context),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _renderTitle(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 25.0,
          ),
          Observer(
            builder: (_) => Text(
                languagetrans.strings.getString("LOGIN_NL_WALLET"),
                style: CommonStyles.title()),
          )
        ],
      ),
    );
  }

  Widget _renderTextInputs(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          TextInput(
            placeholder: "TYPE_EMAIL_OR_PHONE",
            type: TextInputType.emailAddress,
            controller: emailOrPhoneController,
            decoration: isEmailOrPhoneValidated
                ? CommonStyles.normalTextInput()
                : CommonStyles.errorTextInput(),
          ),
          TextInput(
            placeholder: "TYPE_PASSWORD",
            type: TextInputType.emailAddress,
            controller: passwordController,
            decoration: isPasswordValidated
                ? CommonStyles.normalTextInput()
                : CommonStyles.errorTextInput(),
            obscureText: true,
          ),
        ],
      ),
    );
  }

  Widget _renderLoginButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 28.0),
      child: ButtonOrange(
        text: "LOGIN",
        onPress: () {
          _login(context);
        },
        isReady: true,
      ),
    );
  }

  Widget _renderOptions(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.8,
      height: MediaQuery.of(context).size.height / 15,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          TouchableOpacity(
            child: Observer(
              builder: (_) => Text(
                languagetrans.strings.getString("FORGOT_PASSWORD"),
                style: CommonStyles.textNormal(),
              ),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ForgotPasswordScreen()),
              );
            },
          ),
          CustomCheckBox(
            text: "REMEMBER_ACCOUNT",
            isChecked: true,
            textStyle: CommonStyles.textNormal(),
          )
        ],
      ),
    );
  }

  Widget _renderSignUpButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 28.0),
      child: ButtonOrange(
        text: "CREATE_ACCOUNT",
        onPress: () {
          // if (!Validator.isEmail(emailOrPhoneController.text)) {
          //   print("this is not email");
          // }
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SignUpScreen()),
          );
        },
        isReady: false,
        color: Colors.white,
      ),
    );
  }
}

class TextInput extends StatefulWidget {
  String placeholder;
  TextInputType type;
  bool obscureText;
  TextEditingController controller;
  BoxDecoration decoration;

  TextInput(
      {@required this.placeholder,
      @required this.controller,
      this.decoration,
      this.type,
      this.obscureText});

  @override
  _TextInput createState() => _TextInput(
        placeholder: this.placeholder,
        controller: this.controller,
        decoration: this.decoration,
        type: this.type,
        obscureText: this.obscureText,
      );
}

class _TextInput extends State<TextInput> {
  String placeholder;
  TextInputType type;
  bool obscureText;
  TextEditingController controller;
  BoxDecoration decoration;

  _TextInput(
      {@required this.placeholder,
      @required this.controller,
      this.decoration,
      this.type,
      this.obscureText});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.8;
    double height = MediaQuery.of(context).size.height / 15;
    return Container(
      width: width,
      height: height,
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      margin: EdgeInsets.only(bottom: 5.0),
      decoration: decoration,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Observer(
            builder: (_) => Container(
              width: width * 0.93,
              child: TextFormField(
                controller: controller,
                style: CommonStyles.textNormal(),
                decoration: InputDecoration(
                  hintText: languagetrans.strings.getString(placeholder),
                  hintStyle: CommonStyles.placeholderNormal(),
                  border: InputBorder.none,
                ),
                textAlign: TextAlign.center,
                textCapitalization: (type == null || type == TextInputType.text)
                    ? TextCapitalization.words
                    : TextCapitalization.none,
                keyboardType: (type == null || type == TextInputType.text)
                    ? TextInputType.text
                    : type,
                obscureText: (obscureText == null || obscureText == false)
                    ? false
                    : true,
                validator: (value) {
                  if (value == null || value == "") {
                    return "You can't leave that field empty";
                  } else if (type == TextInputType.emailAddress &&
                      !Validator.isEmail(controller.text)) {
                    return "This is not an email";
                  }
                  return null;
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
