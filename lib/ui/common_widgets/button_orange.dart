import 'package:app_ngan_luong/main.dart';
import 'package:app_ngan_luong/utils/custom_colors.dart';
import 'package:flutter/material.dart';
import 'package:app_ngan_luong/utils/common_styles.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:touchable_opacity/touchable_opacity.dart';

class ButtonOrange extends StatefulWidget {
  String text;
  GestureTapCallback onPress;
  bool isReady;
  Color color;

  ButtonOrange({@required this.text, @required this.onPress, this.isReady, this.color});

  @override
  _ButtonOrange createState() => _ButtonOrange(
    text: this.text,
    onPress: this.onPress,
    isReady: this.isReady,
    color: this.color,
  );
}

class _ButtonOrange extends State<ButtonOrange> {
  String text;
  GestureTapCallback onPress;
  bool isReady;
  Color color;

  _ButtonOrange({@required this.text, @required this.onPress, this.isReady, this.color});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height / 15;
    double width = MediaQuery.of(context).size.width * 0.8;

    return TouchableOpacity(
        onTap: onPress,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 28.0),
          height: height,
          width: width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              color: (color != null)
                  ? color
                  : (isReady || isReady == null)
                      ? CustomColor.primaryColor
                      : CustomColor.disabledColor,
              boxShadow: <BoxShadow>[CommonStyles.shadowNormal()]),
          child: Center(
              child: Observer(
            builder: (_) => Text(
              languagetrans.strings.getString(text).toUpperCase(),
              style: (color != Colors.white)
                  ? CommonStyles.textExtraLargeWhite()
                  : CommonStyles.textExtraLargeBlack(),
            ),
          )),
        ));
  }
}
