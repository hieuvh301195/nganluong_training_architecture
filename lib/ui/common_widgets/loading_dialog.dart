import 'package:flutter/material.dart';
import 'package:app_ngan_luong/utils/common_styles.dart';
import 'package:app_ngan_luong/utils/custom_colors.dart';

class LoadingDialog extends StatelessWidget {
  final String message;

  LoadingDialog({@required this.message});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Row(
        children: <Widget>[
          CircularProgressIndicator(
            backgroundColor: CustomColor.primaryColor,
          ),
          Text(
            message,
            style: CommonStyles.textNormal(),
          )
        ],
      )
    );
  }
}
