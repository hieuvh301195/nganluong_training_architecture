import 'package:app_ngan_luong/main.dart';
import 'package:flutter/material.dart';
import 'package:align_positioned/align_positioned.dart';
import 'package:touchable_opacity/touchable_opacity.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class ChooseLang {
  const ChooseLang({this.lang, this.image});

  final String lang;
  final Image image;
}

const List<ChooseLang> languages = const <ChooseLang>[
  const ChooseLang(
      lang: 'en',
      image: Image(
        image: AssetImage("lib/res/images/login/en.png"),
        height: 28,
        width: 28,
      )),
  const ChooseLang(
      lang: 'vn',
      image: Image(
        image: AssetImage("lib/res/images/login/vn.png"),
        height: 28,
        width: 28,
      )),
];

class ChooseLanguage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 37,
            height: 37,
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TouchableOpacity(
                      onTap: () {
                        languagetrans.setTranEN();
                        languagetrans.getTrans(languagetrans.lang);
                      },
                      child: languages[0].image,
                    )
                  ],
                ),
                Visibility(
                  visible:
                      (languagetrans.lang == languages[0].lang) ? true : false,
                  child: AlignPositioned(
                    alignment: Alignment.topRight,
                    child: Container(
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromRGBO(1, 1, 1, 0.8),
                            blurRadius:
                                0.1, // has the effect of softening the shadow
                            spreadRadius:
                                0.0, // has the effect of extending the shadow
                            offset: Offset(
                              0.0, // horizontal, move right 10
                              0.0, // vertical, move down 10
                            ),
                          ),
                        ],
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                      ),
                      child: Image(
                        image: AssetImage("lib/res/images/login/Check.png"),
                        height: 18,
                        width: 18,
                      ),
                    ),
                  ),
                  replacement: Column(),
                ),
              ],
            ),
          ),
          Container(
            width: 37,
            height: 37,
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TouchableOpacity(
                      onTap: () {
                        languagetrans.setTranVN();
                        languagetrans.getTrans(languagetrans.lang);
                      },
                      child: languages[1].image,
                    ),
                  ],
                ),
                Visibility(
                  visible:
                      (languagetrans.lang == languages[1].lang) ? true : false,
                  child: AlignPositioned(
                    alignment: Alignment.topRight,
                    child: Container(
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.8),
                            blurRadius:
                                0.1, // has the effect of softening the shadow
                            spreadRadius:
                                0.0, // has the effect of extending the shadow
                            offset: Offset(
                              0.0, // horizontal, move right 10
                              0.0, // vertical, move down 10
                            ),
                          ),
                        ],
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                      ),
                      child: Image(
                        image: AssetImage("lib/res/images/login/Check.png"),
                        height: 18,
                        width: 18,
                      ),
                    ),
                  ),
                  replacement: Column(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
