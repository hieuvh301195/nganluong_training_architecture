import 'package:app_ngan_luong/main.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:app_ngan_luong/utils/custom_colors.dart';
import 'package:app_ngan_luong/utils/common_styles.dart';
import 'package:flutter_mobx/flutter_mobx.dart';


class CustomCheckBox extends StatefulWidget {
  String text;
  bool isChecked;
  Diagnosticable textStyle;

  CustomCheckBox({@required this.text, @required this.isChecked, this.textStyle});

  _CustomCheckBox createState() => _CustomCheckBox(
    text: this.text,
    isChecked: this.isChecked,
    textStyle: this.textStyle,
  );
}

class _CustomCheckBox extends State<CustomCheckBox> {
  String text;
  bool isChecked;
  Diagnosticable textStyle;
  GestureDetector onPress;

  _CustomCheckBox({@required this.text, @required this.isChecked, this.textStyle});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double size = 15.0;
    return Container(
        height: MediaQuery.of(context).size.height / 15,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: () async {
                setState(() {
                  isChecked = !isChecked;
                });
              },
              child: Container(
                width: size,
                height: size,
                margin: EdgeInsets.symmetric(horizontal: (textStyle==null)?8.0:5.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(width: 0.5),
                    color:
                    isChecked ? CustomColor.normalTextColor : Colors.white),
                child: isChecked
                    ? Image(
                  image: AssetImage('lib/res/images/login/check_white.png'),
                )
                    : null,
              ),
            ),
            Observer(
              builder: (_) => Text(
                languagetrans.strings.getString(text),
                style: (textStyle==null)?CommonStyles.textSmall():textStyle,
              )
            )
          ],
        ));
  }
}