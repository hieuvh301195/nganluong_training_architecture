
class Validator{
  Validator._();

  var value;

  Validator({this.value});

  static bool isEmail(String value) {
    String regExStatement =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
    RegExp regExp = new RegExp(regExStatement);
    return regExp.hasMatch(value);
  }

  static bool isPhoneNumber(String value){
    String regExStatement = r"^(?:[+0]9)?[0-9]{10,12}$";
    RegExp regExp = new RegExp(regExStatement);
    return regExp.hasMatch(value);
  }
}