import 'package:flutter/material.dart';

import '../ui/screens/splash_screen.dart';
import '../ui/screens/login_screen.dart';
import '../ui/screens/home_screen.dart';

class NganLuongRoute{
  NganLuongRoute._();

  static const String root = '/';
  static const String splash = '/splash';
  static const String login = '/login';
  static const String home = '/home';

  static final routes = <String, WidgetBuilder>{
    root: (BuildContext context) => SplashScreen(),
    login: (BuildContext context) => LoginScreen(),
    home: (BuildContext context) => HomeScreen(),
  };

  static void setHomeScreenAsRoot(context){
    Navigator.pushReplacementNamed(context, home);
  }

  static void setSplashScreenAsRoot(context){
    Navigator.pushReplacementNamed(context, splash);
  }

}