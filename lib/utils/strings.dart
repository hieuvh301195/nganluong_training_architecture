class Strings {
  Strings._();

  List<String> languageArray = ['vn', 'en'];
  String language;

  Strings({this.language});




  String getString(String code) {
    String language = this.language;
    if (language == 'vn') {
      switch (code) {
        case "LOGIN":
          return "ĐĂNG NHẬP";
        case "LOGIN_NL_WALLET":
          return "Đăng nhập ví Ngân Lượng";
        case "TYPE_EMAIL_OR_PHONE":
          return "Nhập email hoặc số điện thoại";
        case "TYPE_PASSWORD":
          return "Nhập mật khẩu";
        case "FORGOT_PASSWORD":
          return "Quên mật khẩu";
        case "REMEMBER_ACCOUNT":
          return "Lưu đăng nhập";
        case "CREATE_ACCOUNT":
          return "Đăng ký tài khoản";
        case "CREATE_ACCOUNT_DESCRIPTION":
          return "Vui lòng đăng ký tên chủ tài khoản trùng tên trên CMND / Hộ chiếu của chủ tài khoản";
        case "YOUR_NAME":
          return "Tên chủ tài khoản";
        case "EMAIL":
          return "Email";
        case "PHONE_NUMBER":
          return "Số điện thoại";
        case "PASSWORD":
          return "Mật khẩu";
        case "CONFIRM_PASSWORD":
          return "Xác nhận mật khẩu";
        case "USE_PHONE_NUMBER_TO_LOGIN":
          return "Sử dụng số điện thoại này để đăng nhập";
        case "AGREE_TO_TERM_AGREEMENT":
          return "Tôi đồng ý với các điều khoản";
        case "CREATE":
          return "Đăng ký";
        case "TYPE_PHONE_NUMBER":
          return "Nhập số điện thoại";
        case "TYPE_EMAIL":
          return "Nhập email";
        case "TYPE_YOUR_NAME":
          return "Nhập tên";
        case "FORGOT_PASSWORD_CHOOSE_METHOD":
          return "Chọn một trong những phương thức sau để lấy lại mật khẩu tài khoản";
        case "FORGOT_PASSWORD_NOTI":
          return "Nhận Email mật khẩu \nQua địa chỉ mail tài khoản";
        case "EMAIL_EMPTY":
          return "Vui lòng nhập email đăng ký";
        case "EMAIL_INVALID":
          return "Email không đúng định dạng";
        case "CONTACT_HELP":
          return "Liên hệ hỗ trợ";
        case "SIGNUP":
          return "Đăng ký";
        case "FORGOT_PASSWORD":
          return "Quên mật khẩu";
        case "REMEMBER_ACCOUNT":
          return "Lưu tài khoản";
      }
    } else if (language == 'en') {
      switch (code) {
        case "LOGIN":
          return "LOGIN";
        case "LOGIN_NL_WALLET":
          return "Login Ngan Luong wallet";
        case "TYPE_EMAIL_OR_PHONE":
          return "Type email or phone number";
        case "TYPE_PASSWORD":
          return "Type your password";
        case "FORGOT_PASSWORD":
          return "Forgot password";
        case "REMEMBER_ACCOUNT":
          return "Remember account";
        case "CREATE_ACCOUNT":
          return "Create account";
        case "CREATE_ACCOUNT_DESCRIPTION":
          return "Please type the same name as the name on your ID Card / passport";
        case "YOUR_NAME":
          return "Your name";
        case "EMAIL":
          return "Email";
        case "PHONE_NUMBER":
          return "Phone number";
        case "PASSWORD":
          return "Password";
        case "CONFIRM_PASSWORD":
          return "Confirm password";
        case "USE_PHONE_NUMBER_TO_LOGIN":
          return "Use this phone number to login";
        case "AGREE_TO_TERM_AGREEMENT":
          return "I agree to terms and agreements";
        case "CREATE":
          return "Create";
        case "TYPE_PHONE_NUMBER":
          return "Type your phone number";
        case "TYPE_EMAIL":
          return "Type your email";
        case "TYPE_YOUR_NAME":
          return "Type your name";
        case "FORGOT_PASSWORD_CHOOSE_METHOD":
          return "Choose one of the following methods to retrieve your account password";
        case "FORGOT_PASSWORD_NOTI":
          return "Receive Email Password \nVia account email address";
        case "EMAIL_EMPTY":
          return "Please enter your registration email";
        case "EMAIL_INVALID":
          return "Email invalidate";
        case "CONTACT_HELP":
          return "Contact help";
        case "SIGNUP":
          return "Sign up";
        case "FORGOT_PASSWORD":
          return "Forgot password";
        case "REMEMBER_ACCOUNT":
          return "Remember account";
      }
    } else {
      return "undefine language";
    }
    return null;
  }
}
