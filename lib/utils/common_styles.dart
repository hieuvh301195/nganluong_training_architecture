import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:app_ngan_luong/utils/custom_colors.dart';

class CommonStyles {
  CommonStyles._();

  static Diagnosticable title() {
    return TextStyle(
        fontSize: 26.0,
        fontFamily: 'NunitoSans',
        color: CustomColor.normalTextColor);
  }

  static Diagnosticable textNormal() {
    return TextStyle(
        fontSize: 14.0,
        fontFamily: 'NunitoSans',
        color: CustomColor.normalTextColor);
  }

  static Diagnosticable textBold() {
    return TextStyle(
        fontSize: 14.0,
        fontFamily: 'NunitoSans',
        color: CustomColor.normalTextColor,
        fontWeight: FontWeight.bold);
  }

  static Diagnosticable textSmall() {
    return TextStyle(
        fontSize: 12.0,
        fontFamily: 'NunitoSans',
        color: CustomColor.normalTextColor);
  }

  static Diagnosticable textLarge() {
    return TextStyle(
        fontSize: 15.0,
        fontFamily: 'NunitoSans',
        color: CustomColor.normalTextColor);
  }

  static Diagnosticable textExtraLargeWhite() {
    return TextStyle(
        color: Colors.white, fontFamily: "NunitoSans", fontSize: 18.0);
  }

  static Diagnosticable textExtraLargeBlack() {
    return TextStyle(
        color: CustomColor.normalTextColor,
        fontFamily: "NunitoSans",
        fontSize: 18.0);
  }

  static Diagnosticable placeholderNormal() {
    return TextStyle(
      fontSize: 14.0,
      fontFamily: 'NunitoSans',
      color: CustomColor.normalPlaceholderColor,
    );
  }

  static BoxShadow shadowNormal() {
    return BoxShadow(
        color: new Color.fromRGBO(0, 0, 0, 0.1),
        offset: Offset(0, 1.0),
        blurRadius: 10.0,
        spreadRadius: 0.1);
  }

  static BoxDecoration normalTextInput() {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[CommonStyles.shadowNormal()]);
  }

  static BoxDecoration errorTextInput() {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[CommonStyles.shadowNormal()],
        border: Border.all(
          color: Colors.red,
          width: 0.6,
        ));
  }
}
