import 'package:mobx/mobx.dart';
import 'package:app_ngan_luong/utils/strings.dart';

part 'languages.g.dart';

class LanguageTrans = _Language with _$LanguageTrans;

abstract class _Language with Store {
  @observable
  String lang = 'vn';

  @observable
  Strings strings = Strings(language: 'vn');

  @action
  void setTranVN() {
    lang = 'vn';
  }
  
  @action
  void setTranEN() {
    lang = 'en';
  }

  @action
  void getTrans(String langCode) {
    strings = Strings(language: langCode);
  }

//  @action getLang(){
//    return this.lang;
//  }
}