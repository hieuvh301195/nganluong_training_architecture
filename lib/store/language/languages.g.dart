// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'languages.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LanguageTrans on _Language, Store {
  final _$langAtom = Atom(name: '_Language.lang');

  @override
  String get lang {
    _$langAtom.context.enforceReadPolicy(_$langAtom);
    _$langAtom.reportObserved();
    return super.lang;
  }

  @override
  set lang(String value) {
    _$langAtom.context.conditionallyRunInAction(() {
      super.lang = value;
      _$langAtom.reportChanged();
    }, _$langAtom, name: '${_$langAtom.name}_set');
  }

  final _$stringsAtom = Atom(name: '_Language.strings');

  @override
  Strings get strings {
    _$stringsAtom.context.enforceReadPolicy(_$stringsAtom);
    _$stringsAtom.reportObserved();
    return super.strings;
  }

  @override
  set strings(Strings value) {
    _$stringsAtom.context.conditionallyRunInAction(() {
      super.strings = value;
      _$stringsAtom.reportChanged();
    }, _$stringsAtom, name: '${_$stringsAtom.name}_set');
  }

  final _$_LanguageActionController = ActionController(name: '_Language');

  @override
  void setTranVN() {
    final _$actionInfo = _$_LanguageActionController.startAction();
    try {
      return super.setTranVN();
    } finally {
      _$_LanguageActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setTranEN() {
    final _$actionInfo = _$_LanguageActionController.startAction();
    try {
      return super.setTranEN();
    } finally {
      _$_LanguageActionController.endAction(_$actionInfo);
    }
  }

  @override
  void getTrans(String langCode) {
    final _$actionInfo = _$_LanguageActionController.startAction();
    try {
      return super.getTrans(langCode);
    } finally {
      _$_LanguageActionController.endAction(_$actionInfo);
    }
  }
}
